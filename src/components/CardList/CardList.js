import React, {useCallback} from 'react';
import {StyleSheet, TouchableOpacity, Image} from 'react-native';
import {fontSize} from '../../styles';
import {useNavigation} from '@react-navigation/native';

import {List} from '../List';

export const CardList = ({data, refreshing, onRefresh}) => {
  const navigation = useNavigation();
  const goToDetails = useCallback(
    card => () => {
      navigation.navigate('Details', {
        card,
      });
    },
    [navigation],
  );

  const renderItem = useCallback(
    ({item}) => {
      return (
        <TouchableOpacity
          style={styles.imageContainer}
          onPress={goToDetails(item)}>
          <Image source={{uri: item.url}} style={styles.image} />
        </TouchableOpacity>
      );
    },
    [goToDetails],
  );

  return (
    <List
      data={data}
      refreshing={refreshing}
      onRefresh={onRefresh}
      renderItem={renderItem}
      numColumns={2}
    />
  );
};

const styles = StyleSheet.create({
  imageContainer: {
    flex: 1,
    width: '100%',
    height: 200,
    padding: 5,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  text: {
    fontSize: fontSize.$medium,
    textAlign: 'center',
  },
});

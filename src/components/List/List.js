import React, {memo} from 'react';
import {FlatList} from 'react-native';

export const List = memo(
  ({data, onRefresh, refreshing, renderItem, ...rest}) => {
    return (
      <FlatList
        data={data}
        renderItem={renderItem}
        onRefresh={onRefresh}
        refreshing={refreshing}
        keyExtractor={item => item.id}
        {...rest}
      />
    );
  },
);

import React, {memo} from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';
import {palette, fontSize} from '../../styles';

export const Button = memo(props => {
  const {title, onPress, ...rest} = props;

  return (
    <TouchableOpacity onPress={onPress} {...rest} style={styles.button}>
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  );
});

const styles = StyleSheet.create({
  button: {
    padding: 10,
    backgroundColor: palette.$primaryColor,
    borderRadius: 5,
  },
  text: {
    fontSize: fontSize.$medium,
    textAlign: 'center',
  },
});

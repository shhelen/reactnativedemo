import React from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';

export const DetailScreen = ({navigation, route}) => {
  const card = route.params.card;

  return (
    <View style={styles.container}>
      <Image source={{uri: card.url}} style={styles.image} resizeMode="cover" />
      <Text>{card.title}</Text>
      <Text>{card.subtitle}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  image: {
    width: '100%',
    height: '80%',
  },
});

import React, {useEffect, useState, useCallback} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Alert} from 'react-native';

import Api from '../../api/Api';
import {CardList} from '../../components';

export const HomeScreen = () => {
  const [searchQuery, setSearchQuery] = useState('cats');
  const [isLoading, setLoading] = useState(false);
  const [cards, setCards] = useState([]);

  const fetchData = useCallback(() => {
    setLoading(true);
    Api.search(searchQuery)
      .then(response => {
        setCards(
          response.results.map(item => ({
            id: item.id,
            alt: item.alt_description,
            url: item.urls.small,
            title: item.description,
            subtitle: item.alt_description,
          })),
        );
        setLoading(false);
      })
      .catch(() => {
        Alert.alert('Error', 'Error occured while loading cards');
        setLoading(false);
      });
  }, [searchQuery]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <SafeAreaView edges={['right', 'bottom', 'left']}>
      <CardList data={cards} refreshing={isLoading} onRefresh={fetchData} />
    </SafeAreaView>
  );
};

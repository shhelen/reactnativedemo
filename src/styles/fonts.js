export const fontSize = {
  $small: 12,
  $medium: 16,
  $large: 18,
  $extraLarge: 22,
};

export const palette = {
  $primaryColor: '#FFCC00',
  $secondaryColor: '#262626',
  $basicDarkColor: '#000000',
  $basicLightColor: '#FFFFFF',
};
